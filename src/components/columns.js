export const COLUMNS = [
    {
		Header: 'Image',
		accessor: 'sImage',
		Cell: (tableProps) => (
			<img
				src={tableProps.row.original.sImage}
				width={70}
			/>
		),
	},

    {
        Header : 'Id',
        accessor : '_id'
    },
    {
        Header : 'Type',
        accessor : 'eType'
    }, 
    {
        Header : 'Title',
        accessor : 'sTitle'
    },
    {
        Header : 'Comments-counts',
        accessor : 'nCommentsCount'
    },
    {
        Header : 'Description',
        accessor : 'sDescription'
    }, 
    {
        Header : 'View-counts',
        accessor : 'nViewCounts'
    },
    {
        Header : 'Updated At',
        accessor : 'dUpdatedAt'
    },
    {
        Header : 'Created At',
        accessor : 'dCreatedAt'
    },
]