import React, {useMemo} from 'react'
import {useTable , usePagination} from 'react-table'
import DATA from'./Data.json'
import { COLUMNS } from './columns'
import './table.css'

export const PaginationTable = () => {
    const columns = useMemo(() => COLUMNS, [])
    const data = useMemo(() => DATA, [])
  
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      page,
      nextPage,
      previousPage,
      canNextPage,
      canPreviosPage,
      pageOptions,
      state,
      prepareRow
    } = useTable( 
        {
      columns,
      data
     },
     usePagination
     )

     const {pageIndex} = state
  
    return (
      <>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map(row => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
        <div>
            <span>
                <strong>
                    {pageIndex + 1} of {pageOptions.length}
                </strong> {' '}
            </span>
            <button onClick={() => previousPage()} disabled={!canPreviosPage}>PreviousPage</button>
            <button onClick={() => nextPage()} disabled={!canNextPage}>NextPage</button>
        </div>
      </>
    )
  }


